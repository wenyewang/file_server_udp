#include <stdio.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>

#define TYPE_RRQ   1
#define TYPE_DATA  2
#define TYPE_ACK   3
#define TYPE_ERROR 4
#define TYPE_LIST  5	// choose if you want it or not.

#define WINDOW_SIZE 5

typedef struct packet {
	unsigned char type;
	unsigned char wndSize_seq;
	char content[0];
}packet;

/**** global variable ****/
int gWndSize = 0;		// current window size
char buffer[512 + sizeof(packet)];
struct sockaddr_in addr;
int sock;
int sock_len, len;
/**** gVar end ****/

/******** packet start *********/
packet* createPacket(unsigned char type, unsigned wndSize_seq, int sizeOfContent, char *bufToCopy)
{
	packet* p = (packet*)malloc(sizeof(packet) + sizeOfContent);
	p->type = type;
	p->wndSize_seq = wndSize_seq;
	memcpy(p->content, bufToCopy, sizeOfContent);
	return p;
}

void printPacket(packet *p)
{
	if(p->type == TYPE_ERROR) {
		printf("%d \n", p->type);
		return ;
	}
	printf("%d %d ", p->type, p->wndSize_seq);
	if(p->type == TYPE_RRQ) {
		for(int i = 0; i < 20; i++) {
			printf("%c", p->content[i]);
			if(p->content[i] == 0)
				break;
		}
	}
	else if(p->type == TYPE_DATA) {
		for(int i = 0; i < 512; i++) {
			printf("%x ", p->content[i]);
		}		
	}
	printf("\n");
}

void printPacket1(packet *p)
{
	char* str = (char*)p;
	for(int i = 0; i < 10; i++)
		printf("%02x ", str[i]);
	printf("\n");
}

int sendPacket(unsigned char type, unsigned wndSize_seq, int sizeOfContent, char *buf)
{
	int re = 0;
	if(type == TYPE_ERROR) {
		char p = TYPE_ERROR;
		re = sendto(sock, &p, 1, 0, (struct sockaddr*)&addr, sizeof(addr));
	}
	else if(type == TYPE_DATA) {
	}
	else if(type == TYPE_ACK) {
		packet* p = createPacket(TYPE_ACK, wndSize_seq, 0, NULL);
		printPacket(p);
		re = sendto(sock, p, sizeof(packet), 0, (struct sockaddr*)&addr, sizeof(addr));
	}
	else if(type == TYPE_RRQ) {
		packet* p = createPacket(TYPE_RRQ, wndSize_seq, sizeOfContent, buf);
		printPacket1(p);
		re = sendto(sock, p, sizeof(packet) + sizeOfContent, 0, (struct sockaddr*)&addr, sizeof(addr));
	}
	if(re < 0)
		printf("error in sendPacket\n");
}
/******** packet end *********/


int main(int argc, char* argv[])
{
    if (argc != 5)
    {
        printf("Usage: %s ip port fromFile toFile\n", argv[0]);
        exit(1);
    }
    printf("This is a UDP client\n");

    if ( (sock=socket(AF_INET, SOCK_DGRAM, 0)) <0)
    {
        perror("socket");
        exit(1);
    }
    
    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(argv[2]));
    addr.sin_addr.s_addr = inet_addr(argv[1]);
    if (addr.sin_addr.s_addr == INADDR_NONE)
    {
        printf("Incorrect ip address!");
        close(sock);
        exit(1);
    }

    char buff[32];
    int len = sizeof(addr);
    int num = 1;
    
    int firstTime = -1;
	if(num == 1) {
		int len, seq = 0;
		size_t size;
		packet* p = (packet*)buffer;
		sendPacket(TYPE_RRQ, WINDOW_SIZE, strlen(argv[3]) + 1, argv[3]);
		FILE* fp = fopen(argv[4], "w");
		do {
			len = recvfrom(sock, buffer, 512 + sizeof(packet), 0, (struct sockaddr*)&addr, (socklen_t*)&len);
			printPacket1(p);
			
			if(len == 1 && buffer[0] == 4) {
				printf("received ERROR\n");
				fclose(fp);
				exit(1);
			}
			
			/// these lines are used to test timeout for 5 times.
			/*
			if(p->wndSize_seq == 20 && firstTime < 10) {
				firstTime++;
			} else
			*/
			/// end of test. delete the else below if you want to complete transfer
			
			if(p->wndSize_seq == seq) {
				size = fwrite(p->content, len - sizeof(packet), 1, fp);
				if(size != 1) {
					printf("write error %s\n", strerror(errno));
				}
				sendPacket(TYPE_ACK, seq, 0, NULL);
				seq++;
			}
		} while(len == 512 + sizeof(packet));
		fclose(fp);
	}
    
    return 0;
}
