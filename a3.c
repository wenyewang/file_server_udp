#include <stdio.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <errno.h>

#pragma pack(1)

#define N 256
#define TIMEOUT_MSEC 3

#define TYPE_RRQ   1
#define TYPE_DATA  2
#define TYPE_ACK   3
#define TYPE_ERROR 4

typedef struct packet {
	unsigned char type;
	unsigned char wndSize_seq;
	char content[0];
}packet;

/**** global variable ****/
int gWndSize = 0;		// current window size
int baseSeq = 0;
int nextSeq = 0;
struct sockaddr_in clientAddr;
char filename[20];
int readCnt = 0;
char t_buf[512];
FILE *fp;
int serverSock;
int sock_len, len;
int sendWindowBoundry = 0;
int lastSize;
pthread_t pid;				// pid of thread to send the file.
int size[N];
packet* sendWindow[N];		//valid seq value is from 0 to 50
timer_t aclock[N];
int timeoutCnt[N];
/**** gVar end ****/

/******** timer start ********/
typedef void (*timeoutCallBack)(union sigval);

/** @brief create Timer. after timeout, would call func, sigval_int is used to identify different timer.
 *
 */
int createTimer(timer_t *timerId, timeoutCallBack func, int sigval_int)
{
    struct sigevent sev;

    sev.sigev_notify = SIGEV_THREAD;
    sev.sigev_signo = SIGRTMIN;
    sev.sigev_value.sival_int = sigval_int;
    sev._sigev_un._sigev_thread._function = func;
    sev._sigev_un._sigev_thread._attribute = NULL;

    /* create timer */
    if (timer_create (CLOCK_REALTIME, &sev, timerId) == -1)
    {
    	printf("error in create timer\n");
        return 1;
    }
/*
    if ((int)(*timerId) == -1)
    	return 2;
    	*/
    return 0;
}

int setTimer(timer_t *timerId, int timeMSec)
{
    struct itimerspec its;

    /* Start the timer */
    its.it_value.tv_sec = timeMSec / 1000;
    its.it_value.tv_nsec = (timeMSec % 1000) * 1000000;

    its.it_interval.tv_sec = 0;
    its.it_interval.tv_nsec = 0;

    if (timer_settime (*timerId, 0, &its, NULL) == -1)
    {
    	printf("error in start timer\n");
        return 1;
    }
    return 0;
}

int delTimer(timer_t *timerId)
{
	int re = timer_delete(timerId);
	if(re != 0)
		printf("error in delete timer\n");
	return re;
}
/******** timer end ********/


/******** packet start *********/
packet* createPacket(unsigned char type, unsigned wndSize_seq, int sizeOfContent, char *bufToCopy)
{
	packet* p = (packet*)malloc(sizeof(packet) + sizeOfContent);
	p->type = type;
	p->wndSize_seq = wndSize_seq;
	memcpy(p->content, bufToCopy, sizeOfContent);
	return p;
}

int sendPacketNoDat(unsigned char type, unsigned wndSize_seq, int sizeOfContent, char *buf)
{
	int re = 0;
	if(type == TYPE_ERROR) {
		char p = TYPE_ERROR;
		re = sendto(serverSock, &p, 1, 0, (struct sockaddr*)&clientAddr, len);
	}
	else if(type == TYPE_ACK) {
		packet* p = createPacket(TYPE_ACK, wndSize_seq, 0, NULL);
		re = sendto(serverSock, &p, sizeof(packet), 0, (struct sockaddr*)&clientAddr, len);
	}
	else if(type == TYPE_RRQ) {
		packet* p = createPacket(TYPE_RRQ, wndSize_seq, sizeOfContent, buf);
		re = sendto(serverSock, &p, sizeof(packet) + wndSize_seq, 0, (struct sockaddr*)&clientAddr, len);
	}
	if(re < 0)
		printf("error in sendPacketNoDat\n");
}

int sendDataPkt(int pos)
{
	int re;
	re = sendto(serverSock, (char*)sendWindow[pos], 2 + size[pos], 0, (struct sockaddr*)&clientAddr, len);
	if(re != (sizeof(packet) + size[pos])) {
		printf(" %d. error in send packet: %s\n", re, strerror(errno));
		printf("%s %d\n", (char*)sendWindow[pos], 2 + size[pos]);
	}
	return re;
}

void printPacket(packet *p)
{
	if(p->type == TYPE_ERROR) {
		printf("%d \n", p->type);
		return ;
	}
	printf("%d %d ", p->type, p->wndSize_seq);
	if(p->type == TYPE_RRQ) {
		for(int i = 0; i < 20; i++) {
			printf("%x ", p->content[i]);
			if(p->content[i] == 0)
				break;
		}
	}
	else if(p->type == TYPE_DATA) {
		for(int i = 0; i < 512; i++) {
			printf("%x ", p->content[i]);
		}		
	}
	printf("\n");
}
/******** packet end *********/


void timeout_handle(union sigval s)
{
	int i = s.sival_int;	// which timer is timeout. aclock[s.sival_int] is timeout.
	int j = (baseSeq + gWndSize) > sendWindowBoundry ? sendWindowBoundry : (baseSeq + gWndSize);
	int re;
	
//	printf("clock time out %d  baseSeq:%d, nextSeq:%d\n", s.sival_int, baseSeq, nextSeq);
	if(i < baseSeq) {
		return ;
	}
	timeoutCnt[i]++;
	if(timeoutCnt[i] >= 5) {
		/// stop transmission if the timer is timeout for five times.
		printf("should Stop Transmission %d baseSeq:%d, nextSeq:%d\n", i, baseSeq, nextSeq);
		exit(1);
	}

	/// retransfer from sendWindow[i] to sendWindow[j]
	while(i != j && i < sendWindowBoundry) {
		printf("resend %d\n", i);
		
		sendDataPkt(i);
		re = setTimer(&aclock[i], TIMEOUT_MSEC);
		if(re != 0) printf("error in set Timer\n");
		i++;
	}
}

int re;

void send_data()
{
//	printf("in test thread\n");
//	printf("%d %d", baseSeq, nextSeq);
	while(nextSeq < baseSeq + gWndSize && nextSeq < sendWindowBoundry) {
		printf(" will send %d \n", nextSeq);
		fflush(stdout);

		sendDataPkt(nextSeq);	///< send sendWindow[nextSeq] to client

		/// start the timer for it. If a timer is timeout, timeout_handle() would be called
		re = setTimer(&aclock[nextSeq], TIMEOUT_MSEC);
		if(re != 0) printf("error in set Timer\n");
		nextSeq++;

//		while(nextSeq - baseSeq == gWndSize || nextSeq == sendWindowBoundry);		///< if window is full, wait.
	}
}


int main(int argc, char* argv[])
{
	int portnumber;
	sock_len = sizeof(struct sockaddr_in);

    if (argc != 2) {
        fprintf(stderr, "Usage:%s portnumber\n\a", argv[0]);
        exit(1);
    }
    // get port number
    if ((portnumber = atoi(argv[1])) < 0) {
        fprintf(stderr, "invalid portnumber\n\a");
        exit(1);
    }

	// fill addr structure
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(portnumber);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

	serverSock = socket(AF_INET,SOCK_DGRAM,0);
	if(serverSock == -1) {
		printf("Server Error, socket established error %s\n", (char*)strerror(errno));
		exit(1);
	}

    if(bind(serverSock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		printf("Server Error, bind error %s\n", (char*)strerror(errno));
		exit(1);
    }
    
    // malloc buffer to decode packet from client.
    char* buffer = (char*)malloc(sizeof(packet) + 512);
    packet* buf = (packet*)buffer;
    int n, re;
    len = sizeof(clientAddr);
    
	while(1) {
		n = recvfrom(serverSock, buffer, sizeof(packet) + 512, 0, (struct sockaddr*)&clientAddr, (socklen_t*)&len);
		if(n > 0) {
			// print the information for debug
			printf("PKT: %d | ", n);
			for(int i = 0; i < n; i++) {
				printf("%02x, ", buffer[i]);
			}
			printf("\n");
			fflush(stdout);
//			printf("%s %u says\n", inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port));
			
			switch(buf->type) {
				case TYPE_RRQ:
					gWndSize = buf->wndSize_seq;		///< get window size
					strcpy(filename, buf->content);		///< get file name the client want to copy
					
					if(strlen(filename) == 0) {			///< if filename equals NULL, return ERROR
						sendPacketNoDat(TYPE_ERROR, 0, 0, 0);
					}
					fp = fopen(filename, "r");
					if(fp == NULL) {					///< if filename doesn't exist, return ERROR
						sendPacketNoDat(TYPE_ERROR, 0, 0, 0);
					}
					else {			///< if filename exist, transfer files.
						sendWindowBoundry = 0;
						/** 
						 *  Put everything into global variables. Pkts, timer, Datasize.
						 *  then create a thread to send from the global variable.
						 */
						do {
							readCnt = fread(t_buf, 1, 512, fp);
							size[sendWindowBoundry] = readCnt;

							// set timer
							re = createTimer(&aclock[sendWindowBoundry], timeout_handle, sendWindowBoundry);
							if(re != 0) printf("error in create Timer %d\n", sendWindowBoundry);

							// set DATA packet
							packet* p = createPacket(TYPE_DATA, sendWindowBoundry, readCnt, t_buf);
							sendWindow[sendWindowBoundry++] = p;
							lastSize = readCnt;
						} while(readCnt == 512);
						
						printf("%d packets created lastDat %d\n", sendWindowBoundry, lastSize);
						
						memset(timeoutCnt, 0, sizeof(timeoutCnt));
						nextSeq = 0;
						baseSeq = 0;
						send_data();
					}
					break;
				case TYPE_ACK:
					if(buf->wndSize_seq == baseSeq) {
						re = setTimer(&aclock[baseSeq], 0);		//close coresponding timer, when get an ack
						if(re != 0) printf("error in set Timer\n");
						baseSeq++;
					}
					send_data();
					break;
				case TYPE_ERROR:
					break;
				default:
					printf("error type received %d\n", buf->type);	
					break;						
			}
		}
		else {
			printf("error\n");
		}
	}
    return 0;
}
